package cz.cesnet.cloud;

import cz.cesnet.cloud.occi.api.Client;
import cz.cesnet.cloud.occi.api.exception.CommunicationException;
import cz.cesnet.cloud.occi.api.http.HTTPClient;
import java.net.URI;

public class Example {

public static void main(String[] args) {
	try {
		Client client = new HTTPClient(URI.create("https://remote.server.net"));
		client.connect();
	} catch (CommunicationException ex) {
		throw new RuntimeException(ex);
	}
}

}
