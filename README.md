# Ubuntu 14

    apt-get install maven-debian-helper
    rm -rf ~/.m2
    mvn-debian compile -Dcore.version=0.2.4 -Dapi.version=0.2.6

# RedHat

    yum install maven-local
    rm -rf ~/.m2
    xmvn compile
